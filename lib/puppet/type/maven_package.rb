class String
	#	According to the org.apache.maven.project.validation.DefaultModelValidator
	#	class (see
	#	"http://svn.apache.org/repos/asf/maven/maven-2/branches/maven-2.2.x/maven-project/src/main/java/org/apache/maven/project/validation/DefaultModelValidator.java"),
	#	a valid Maven identifier is a non-empty string of characters from the set
	#
	#	  	ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.
	#
	#	I return +true+ iff I am a valid Maven identifier.
	def maven_id?
		return(self.match(/\A[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_\-.]+\Z/))
	end
end

Puppet::Type.newtype(:maven_package) do
	@doc = '
		Attempt to download (possibly as an authenticated user) a Maven	artifact
		comprising a compressed archive from a Maven repository via HTTP or HTTPS
		and install the contents of the archive into an installation directory on
		the local host. Currently, this type can only download Maven release
		artifacts, and can only install `gzip` compressed `tar` archives. See the
		README.md file at the root of the type directory for more details.'

	newparam(:name, :namevar => true) do
		desc('
			A name to identify the Puppet resource. Required.')
	end

	newproperty(:state) do
		desc('
			The state of the installation directory. One of

			*	`absent` - the installation directory is empty.

			*	`corrupt` - the installation directory is not empty but the
				`current` subdirectory does not contain the contents of an archive (the
				`.version` file of the `current` subdirectory is unreadable). The
				resource uses this state to ensure that the installation directory is
				empty if a user specifies `state => absent`. It is not meant for use
				by a user, but if a user *does* specify `state => corrupt` then nothing
				happens.

			*	`present` - the `current` subdirectory contains the contents of an
				archive (the `.version` file of the `current` subdirectory is
				readable).

			*	`up_to_date` - the `current` subdirectory contains the contents of
				an up-to-date archive (the `.version` file of the `current`
				subdirectory contains the version string of the artifact that provides
				the latest version of the archive).')
		self.newvalues(:absent, :corrupt, :present, :up_to_date)
	end

	newparam(:install_dir_path) do
		desc('
			A path to the installation directory. Required if state is set.')
		self.munge() do |value|
			return(Pathname.new(value).expand_path())
		end
	end

	newparam(:install_user) do
		desc('
			The name or UID of the local user that owns the `current` subdirectory
			of the installation directory and all of its contents.')
	end

	newparam(:install_group) do
		desc('
			The name or GID of the local group that owns the `current` subdirectory
			of the installation directory and all of its contents.')
	end

	newparam(:repo_uri) do
		desc('
			The URI of the repository. Required if state is "present" or
			"up_to_date".')
		self.munge() do |value|
			uri = URI.parse(value)
			raise(ArgumentError, "bad URI(is not HTTP or HTTPS URI?): #{value}") if not uri.is_a?(URI::HTTP)
			return(uri)
		end
	end

	newparam(:repo_user_name) do
		desc('
			The name of the authenticated repository user.')
	end

	newparam(:repo_user_password) do
		desc('
			The password of the authenticated repository user.')
	end

	newparam(:group_id) do
		desc('
			The group identifier of the artifact. Required if state is "present" or
			"up_to_date".')
		self.validate() do |value|
			raise(ArgumentError, "invalid Maven identifier: #{value}") if not value.maven_id?
		end
	end

	newparam(:artifact_id) do
		desc('
			The artifact identifier of the artifact. Required if state is "present"
			or "up_to_date".')
		self.validate() do |value|
			raise(ArgumentError, "invalid Maven identifier: #{value}") if not value.maven_id?
		end
	end

	newparam(:classifier) do
		desc('
			The classifier of the artifact.')
	end

	newparam(:packaging) do
		desc('
			The packaging of the artifact. If this parameter is not given then the
			packaging is deduced from the repository.')
	end

	newparam(:strip_components) do
		desc('
			The number of leading directory components to strip from the unpacked
			archive. The default is 0.')
	end

	#	XXX: Experimental.
	newparam(:execute) do
		self.newvalues(:never, :immediately, :on_notification)
	end

	#	XXX:Experimental.
	def refresh
		provider.refresh()
	end
end