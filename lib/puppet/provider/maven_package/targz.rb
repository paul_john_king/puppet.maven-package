#	Encoding: utf-8

#	TODO: Understand and, if necessary, implement self.prefetch.

#	TODO: Better name and document the experimental code here and in the README
#	and the type.

class Pathname
	#	If I am a directory then I am empty.
	def empty_if_dir?
		return(!self.directory? || self.children().empty?)
	end

	#	If I am a directory then delete all of my contents, but not me.
	def empty()
		self.each_child(){|child| child.rmtree()} if self.directory?
	end

	#	If I am a file then return my contents as a string, otherwise return +nil+.
	def read_string()
		return(self.file? ? self.read() : nil)
	end

	#	If I am a file then overwrite my contents with a string.
	def write_string(string)
		self.open("w") do |file|
			file << string
		end
	end

	#	Set the user and group identifiers of me and all of my components.  If an
	#	user/group identifier is +nil+ then leave the user/group ownership unchanged.
	def set_owner(uid, gid)
		self.find() do |file|
			file.chown(uid, gid)
		end
	end
end

Puppet::Type.type(:maven_package).provide(:targz) do
	#	I declare myself an unsuitable provider if my Ruby requirements are not met.
	confine :true => begin
		begin
			require "nokogiri"
			require "httpclient"
			true
		rescue LoadError
			false
		end
	end

	#	I use an external tar command because I have found no native Ruby tar
	#	implementation that can handle long paths
	self.commands(:tar => "tar")

	#	I do not maintain a record of resources I can manage.  I therefore implement
	#	a vacuous instance discovery class method.
	def self.instances()
		return([])
	end

	#	Strictly speaking, the 'state()' method should implement the logic
	#
	#		if the archive is installed:
	#
	#		    if the archive is up to date:
	#
	#		        if state => absent:        return :up_to_date
	#		        if state => present:       return :up_to_date
	#		        if state => up_to_date:    return :up_to_date
	#
	#		    if the archive is not up to date:
	#
	#		        if state => absent:        return :present
	#		        if state => present:       return :present
	#		        if state => up_to_date:    return :present
	#
	#		if the archive is not installed:
	#
	#		    if state => absent:            return :absent
	#		    if state => present:           return :absent
	#		    if state => up_to_date:        return :absent
	#
	#	Notice, however, two practical shortcomings of this logic. Firstly, if
	#
	#	*	the archive is installed,
	#	*	the archive is up to date, and
	#	*	the user specifies 'state => present'
	#
	#	then for each execution of the manifest, the logic returns ':up_to_date',
	#	which signals a state mismatch, which in turn triggers an unnecessary
	#	re-installation of the already installed up-to-date archive. Secondly, if
	#
	#	*	the archive is installed, and
	#	*	the user specifies 'state => absent'
	#
	#	then for each execution of the manifest, the logic needlessly examines the
	#	Maven repository just to determine whether to return ':present' or
	#	':up_to_date', even though returning either signals a state mismatch, which
	#	in turn triggers a de-installation of the currently installed archive. This
	#	is not only inefficient. If the repository happens to be empty then the
	#	execution will throw an exception and not de-install the archive.
	#	Consequently, the 'state()' method could instead implement the logic
	#
	#		if the archive is installed:
	#
	#		    if state => absent:                return :present
	#		    else:
	#
	#		        if the archive is up to date:
	#
	#		            if state => present:       return :present
	#		            if state => up_to_date:    return :up_to_date
	#
	#		        if the archive is not up to date:
	#
	#		            if state => present:       return :present
	#		            if state => up_to_date:    return :present
	#
	#		if the archive is not installed:
	#
	#		    if state => absent:                return :absent
	#		    if state => present:               return :absent
	#		    if state => up_to_date:            return :absent
	#
	#	Logically, the only differences between the two logics are that if
	#
	#	*	the archive is installed,
	#	*	the archive is up to date, and
	#	*	the user specifies 'state => present'
	#
	#	or
	#
	#	*	the archive is installed,
	#	*	the archive is up to date, and
	#	*	the user specifies 'state => absent'
	#
	#	then for each execution of the manifest, the logic now returns ':present'
	#	rather than ':up_to_date'. This is still true -- an up-to-date installation
	#	is also a present installation -- and makes no practical difference other
	#	than avoiding an unnecessary re-installation of the archive or an unnecessary
	#	examination of the Maven repository.
	#
	#	Now, the method deems an archive installed if it can read the content of the
	#	'.version' file in the 'current' subdirectory of the installation directory.
	#	Thus, it naïvely deems an archive installed even if the '.version' file is
	#	the only file in the 'current' subdirectory, and deems an archive not
	#	installed if the 'current' directory contains all of the contents of an
	#	archive, but merely lacks the '.version' file. In effect, the method trusts
	#	the host system to maintain the integrity of an installed archive. However,
	#	the method *does* want to be sure that if the user specifies 'state =>
	#	absent' then the installation directory must be entirely empty (though it
	#	does not delete the installation directory itself because the directory might
	#	be a file-system mount point). To this end, the method introduces a 'corrupt'
	#	state. If
	#
	#	*	the user specifies 'state => absent',
	#	*	the 'current/.version' file is readable, but
	#	*	the installation directory is not empty
	#
	#	then the method returns ':corrupt', thus triggering a state change during
	#	which the 'state=(state)' method erases the contents of the installation
	#	directory.  The user should never specify 'state => corrupt', but if they
	#	do, the method returns ':corrupt', thus no state changes and no actions are
	#	triggered.
	#
	#	Combining these, the 'state()' method actually implements the logic
	#
	#		if the archive is installed:
	#
	#		    if state => absent:                                return :present
	#		    else:
	#
	#		        if the archive is up to date:
	#
	#		            if state => present:                       return :present
	#		            if state => up_to_date:                    return :up_to_date
	#
	#		        if the archive is not up to date:
	#
	#		            if state => present:                       return :present
	#		            if state => up_to_date:                    return :present
	#
	#		if the archive is not installed:
	#
	#		    if state => absent:
	#
	#		        if the installation directory is empty:        return :absent
	#		        if the installation directory is not empty:    return :corrupt
	#
	#		    if state => present:                               return :absent
	#		    if state => up_to_date:                            return :absent
	#
	#		if state => corrupt:                                   return :corrupt
	#
	#	This same logic can however be reformulated more efficiently as
	#
	#		if the archive is installed:
	#
	#		    if state => absent:                                return :present
	#		    if state => present:                               return :present
	#		    if state => up_to_date:
	#
	#		        if the archive is up to date:                  return :up_to_date
	#		        if the archive is not up to date:              return :present
	#
	#		if the archive is not installed:
	#
	#		    if state => absent:
	#
	#		        if the installation directory is empty:        return :absent
	#		        if the installation directory is not empty:    return :corrupt
	#
	#		    if state => present:                               return :absent
	#		    if state => up_to_date:                            return :absent
	#
	#		if state => corrupt:                                   return :corrupt
	#
	#	or even more efficiently as
	#
	#		if state => absent:
	#
	#		    if the archive is installed:                       return :present
	#		    if the archive is not installed:
	#
	#		        if the installation directory is empty:        return :absent
	#		        if the installation directory is not empty:    return :corrupt
	#
	#		if state => present:
	#
	#		    if the archive is installed:                       return :present
	#		    if the archive is not installed:                   return :absent
	#
	#		if state => up_to_date:
	#
	#		    if the archive is installed:
	#
	#		        if the archive is up to date:                  return :up_to_date
	#		        if the archive is not up to date:              return :present
	#
	#		    if the archive is not installed:                   return :absent
	#
	#		if state => corrupt:                                   return :corrupt
	#
	def state()
		#	XXX: Experimental begin.
		@state  = @resource[:state]
		@execute = @resource[:execute]
		return(@state) if @execute == :on_notification
		@install_dir_path = @resource[:install_dir_path]
		@install_dir_path || raise("Parameter 'install_dir_path' failed on resource '#{@resource}': missing value.")
		#	XXX: Experimental end.
		@current_dir_path          = @install_dir_path + "current"
		@back_up_dir_path          = @install_dir_path + "back_up"
		@staging_dir_path          = @install_dir_path + "staging"
		@staging_file_path         = @install_dir_path + "staging.tgz"
		@current_version_file_path = @current_dir_path + ".version"
		@staging_version_file_path = @staging_dir_path + ".version"
		@install_user              = self.uid(@resource[:install_user])
		@install_group             = self.gid(@resource[:install_group])
		@repo_uri                  = @resource[:repo_uri]
		@repo_user_name            = @resource[:repo_user_name]
		@repo_user_password        = @resource[:repo_user_password]
		@group_id                  = @resource[:group_id]
		@artifact_id               = @resource[:artifact_id]
		@classifier                = @resource[:classifier]
		@packaging                 = @resource[:packaging]
		@strip_components          = @resource[:strip_components] || "0"
		@current_version           = @current_version_file_path.read_string()
		#	I implement the following logic, described earlier,
		#
		#		if state => absent:
		#
		#		    if the archive is installed:                       return :present
		#		    if the archive is not installed:
		#
		#		        if the installation directory is empty:        return :absent
		#		        if the installation directory is not empty:    return :corrupt
		#
		#		if state => present:
		#
		#		    if the archive is installed:                       return :present
		#		    if the archive is not installed:                   return :absent
		#
		#		if state => up_to_date:
		#
		#		    if the archive is installed:
		#
		#		        if the archive is up to date:                  return :up_to_date
		#		        if the archive is not up to date:              return :present
		#
		#		    if the archive is not installed:                   return :absent
		#
		#		if state => corrupt:                                   return :corrupt
		#
		case @state
		when :absent
			if @current_version
				return :present
			else
				if @install_dir_path.empty_if_dir?
					return(:absent)
				else
					return(:corrupt)
				end
			end
		when :present
			if @current_version
				return(:present)
			else
				self.create_communication_properties()
				return(:absent)
			end
		when :up_to_date
			self.create_communication_properties()
			if @current_version
				if @current_version == @latest_version
					return(:up_to_date)
				else
					return(:present)
				end
			else
				return(:absent)
			end
		when :corrupt
			return(:corrupt)
		end
	end

	#	Since the 'state()' method implements the logic
	#
	#		if the archive is installed:
	#
	#		    if state => absent:                                return :present
	#		    else:
	#
	#		        if the archive is up to date:
	#
	#		            if state => present:                       return :present
	#		            if state => up_to_date:                    return :up_to_date
	#
	#		        if the archive is not up to date:
	#
	#		            if state => present:                       return :present
	#		            if state => up_to_date:                    return :present
	#
	#		if the archive is not installed:
	#
	#		    if state => absent:
	#
	#		        if the installation directory is empty:        return :absent
	#		        if the installation directory is not empty:    return :corrupt
	#
	#		    if state => present:                               return :absent
	#		    if state => up_to_date:                            return :absent
	#
	#		if state => corrupt:                                   return :corrupt
	#
	#	the only possible changes of state are
	#
	#	*	from 'corrupt' to 'absent'     if the installation directory is not empty,
	#	*	from 'present' to 'absent'     if the archive is installed,
	#	*	from 'absent'  to 'present'    if the archive is not installed,
	#	*	from 'absent'  to 'up_to_date' if the archive is not installed, and
	#	*	from 'present' to 'up_to_date' if the archive is installed and out of date.
	#
	#	Consequently, the 'state=(«state»)' method implements the logic
	#
	#		if «state» is :absent:
	#
	#			delete the contents of the installation directory
	#
	#		if «state» is :present or :up_to_date:
	#
	#			back up the currently installed archive (if any) and
	#			install the latest version of the archive
	#
	def state=(state)
		#	XXX: Experimental begin.
		return() if @execute == :never
		#	XXX: Experimental end.
		case state
		when :absent
			@install_dir_path.empty()
		when :present, :up_to_date
			@install_dir_path.mkpath()
			#	I download the latest version of the artifact to a staging file.
			@staging_file_path.rmtree() if @staging_file_path.exist?
			@staging_file_path.parent().mkpath()
			uri = @artifact_version_dir_uri.dup()
			packaging = @packaging || self.packaging(@http_client, @artifact_version_dir_uri, @artifact_id, @latest_version)
			uri.path = "#{uri.path}/#{@latest_version}/#{@artifact_id}-#{@latest_version}%s.#{packaging}" % ("-#{@classifier}" if not @classifier.nil?)
			begin
				@staging_file_path.open("wb") do |file|
					@http_client.get_content(uri) do |chunk|
						file << chunk
					end
				end
			rescue Exception => exception
				raise("Download failed on resource '#{@resource}': #{exception}.")
			end
			#	I unpack the staging file to a staging directory and delete the staging file.
			@staging_dir_path.rmtree() if @staging_dir_path.exist?
			@staging_dir_path.mkpath()
			tar("-xzf", @staging_file_path, "-C", @staging_dir_path, "--strip-components", @strip_components)
			@staging_file_path.delete()
			#	I write the version of the artifact into a version file in the staging
			#	directory.
			@staging_version_file_path.write_string(@latest_version)
			#	I set the ownership of the staging directory.
			@staging_dir_path.set_owner(@install_user, @install_group)
			#	I rotate the staging, current and back-up directories.
			@back_up_dir_path.rmtree()                  if @back_up_dir_path.exist?
			@current_dir_path.rename(@back_up_dir_path) if @current_dir_path.exist?
			@staging_dir_path.rename(@current_dir_path) if @staging_dir_path.exist?
		end
	end

	#	If
	#
	#	*	«user» is the name or identifier of a local host user
	#
	#	then
	#
	#		self.uid(«user»)
	#
	#	return the identifier of the user.
	def uid(user)
		begin
			id = (Integer(user))
		rescue Exception
			return(Etc.getpwnam(user)[:uid])
		end
		return(Etc.getpwuid(id)[:uid])
	end

	#	If
	#
	#	*	«group» is the name or identifier of a local host group
	#
	#	then
	#
	#		self.uid(«group»)
	#
	#	return the identifier of the group.
	def gid(group)
		begin
			id = (Integer(group))
		rescue Exception
			return(Etc.getgrnam(group)[:gid])
		end
		return(Etc.getgrgid(id)[:gid])
	end

	#	If I must communicate with a Maven server then
	#
	#		self.create_communication_properties()
	#
	#	checks that the required parameters have been set and creates some necessary
	#	properties.
	def create_communication_properties()
		@repo_uri           || raise("Parameter 'repo_uri' failed on resource '#{@resource}': missing value.")
		@repo_user_name     || raise("Parameter 'repo_user_name' failed on resource '#{@resource}': missing value.")
		@repo_user_password || raise("Parameter 'repo_user_password' failed on resource '#{@resource}': missing value.")
		@group_id           || raise("Parameter 'group_id' failed on resource '#{@resource}': missing value.")
		@artifact_id        || raise("Parameter 'artifact_id' failed on resource '#{@resource}': missing value.")
		@http_client              = self.http_client(@repo_uri, @repo_user_name, @repo_user_password)
		@artifact_version_dir_uri = self.artifact_version_dir_uri(@repo_uri, @group_id, @artifact_id)
		@latest_version           = self.latest_version(@http_client, @artifact_version_dir_uri)
	end

	#	If
	#
	#	*	«uri» is the HTTP or HTTPS URI of a realm, and
	#
	#	*	«user_name» and «user_password» are the name (possibly +nil+) and password
	#	 	(possibly +nil+) of a user
	#
	#	then
	#
	#		self.http_client(«uri», «user_name», «user_password»)
	#
	#	returns an HTTP client.  If neither the name nor the password of the user are
	#	+nil+ then the client provides each request it makes with
	#	basic-authentication for the user to the realm.
	def http_client(uri, user_name, user_password)
		client = HTTPClient.new()
		client.set_auth(uri, user_name, user_password)
		client.ssl_config().ssl_version = :SSLv23
		return(client)
	end

	#	If
	#
	#	*	«repo_uri» is the URI of a Maven repository, and
	#
	#	*	«group_id» and «artifact_id» are the Maven group and artifact identifiers
	#	 	of an artifact the repository holds
	#
	#	then
	#
	#		self.artifact_version_dir_uri(«repo_uri», «group_id», «artifact_id»)
	#
	#	returns the URI of the artifact version directory for the archive.
	def artifact_version_dir_uri(repo_uri, group_id, artifact_id)
		uri = repo_uri.dup()
		uri.path = "#{uri.path}/#{group_id.gsub('.','/')}/#{artifact_id}"
		return(uri)
	end

	#	The content of a Maven metadata file for an artifact has the form
	#
	#		<metadata>
	#		  <groupId>«group_id»</groupId>
	#		  <artifactId>«artifact_id»</artifactId>
	#		  <versioning>
	#		    <release>«version_i»</release>
	#		    <versions>
	#		      <version>«version_1»</version>
	#		      <version>«version_2»</version>
	#		      <version>«version_3»</version>
	#		      <version>«version_4»</version>
	#		                    ⋮
	#		  </versions>
	#		  <lastUpdated>«time_stamp»</lastUpdated>
	#		  </versioning>
	#		</metadata>
	#
	#	where
	#
	#	*	«version_1», «version_2», «version_3», «version_4»,‥‥  are the
	#	 	versions of the artifact that the repository currently holds or formerly
	#	 	held,
	#
	#	*	«version_i» is the latest version (according to the Maven versioning
	#	 	conventions) of the artifact that the repository currently holds, and
	#
	#	*	«time_stamp» is a time stamp (in
	#	 	«year»«month»«day»«hour»«minute»«second» format) of the last time a
	#	 	version of the artifact was updated on the repository.
	#
	#	If
	#
	#	*	«http_client» is a HTTP/HTTPS client, and
	#
	#	*	«artifact_version_dir_uri» is a URI for the artifact version directory of a
	#	 	Maven artifact
	#
	#	then
	#
	#		self.latest_version(«http_client», «artifact_version_dir_uri»)
	#
	#	extracts the name of the latest Maven version of the artifact from the
	#	metadata file for the artifact with the authority of «http_client», and
	#	returns the version as a string.
	def latest_version(http_client, artifact_version_dir_uri)
		uri = artifact_version_dir_uri.dup()
		uri.path = "#{uri.path}/maven-metadata.xml"
		text = Nokogiri::XML(http_client.get_content(uri)).xpath("/metadata/versioning/release/text()")
		raise("no <release> element found in Maven matadata file in '#{uri}'.") if text.empty?
		return(text.to_s()	)
	rescue Exception => exception
		raise("Download failed on resource '#{@resource}': #{exception}.")
	end

	#	A Maven POM file for an artifact contains '<project>' subelements of the form
	#
	#	*	<groupId>«group_id»</groupId>,
	#
	#	*	<artifactId>«artifact_id»</artifactId>,
	#
	#	*	<version>«version»</version>, and
	#
	#	*	<packaging>«packaging»</packaging>.
	#
	#	If
	#
	#	*	«http_client» is a HTTP/HTTPS client,
	#
	#	*	«artifact_version_dir_uri» is a URI for the artifact version directory of a
	#	 	Maven artifact, and
	#
	#	*	«artifact_id» and «version» are the Maven artifact identifier of the
	#	 	artifact
	#
	#	then
	#
	#		self.packaging(«http_client», «artifact_version_dir_uri», «artifact_id»,
	#			«version»)
	#
	#	extracts the Maven packaging of the artifact from the POM file for the
	#	artifact with the authority of «http_client», and returns the packaging as a
	#	string.
	def packaging(http_client, artifact_version_dir_uri, artifact_id, version)
		uri = artifact_version_dir_uri.dup()
		uri.path = "#{uri.path}/#{version}/#{artifact_id}-#{version}.pom"
		text = Nokogiri::XML(http_client.get_content(uri)).xpath("/xmlns:project/xmlns:packaging/text()")
		raise("no <packaging> element found in '#{uri}'.") if text.empty?
		return(text.to_s)
	rescue Exception => exception
		raise("Download failed on resource '#{@resource}': #{exception}.")
	end

	#	XXX: Experimental.
	def refresh()
		if @execute == :on_notification
			@resource[:execute] = :immediately
			self.state=(@state) if self.state() != @state
		end
	end
end
