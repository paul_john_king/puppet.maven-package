# maven_package

## Description

This type attempts to download (possibly as an authenticated user) a Maven
artifact comprising a compressed archive from a Maven repository via HTTP or
HTTPS and install the contents of the archive into an installation directory on
the local host.  Currently, this type can only download Maven release
artifacts, and can only install `gzip` compressed `tar` archives.

A Maven repository is a directory containing a structure of subdirectories and
files named for the artifacts the repository holds. Suppose that the repository
holds a release artifact with

*	group identifier `«group_id»`,

*	artifact identifier `«artifact_id»`,

*	version `«version»`,

*	packaging `«packaging»`, and

*	(optional) classifier `«classifier»`

and that

*	`«group_path»`  is `«group_id»` with each occurrence of the `.` character
	replaced by the `/` path separator.

The path `«group_path»/«artifact_id»` from the repository root names the
*artifact version directory* of the artifact.  This directory contains a Maven
metadata file named `maven-metadata.xml` with MD5 and SHA1 digests, and a
subdirectory for each version of the artifact the repository currently holds,
named after the version.  The name of the latest version (according to the
Maven versioning conventions) can be extracted from the metadata file.  The
path `«group_path»/«artifact_id»/«version»` from the repository root names the
*artifact directory* of version `«version»` of the artifact.  This directory
contains a Maven POM file named `«artifact_id»-«version».pom` with MD5 and SHA1
digests, and the artifact itself - named
`«artifact_id»-«version»-«classifier».«packaging»` if the artifact has a
classifier or `«artifact_id»-«version».«packaging»` otherwise - also with MD5
and SHA1 digests.  The packaging of the artifact can be extracted from the POM file,
unless the artifact is, for example, a Maven assembly attached to another artifact.

Some Maven repository servers, such as a [Sonatype
Nexus](http://www.sonatype.org/nexus/ "Sonatype Nexus OSS") server, provide
access to artifacts via HTTP or HTTPS.  Each repository on a Nexus server has a
name and an identifier.  A Nexus server with URI `«nexus»` provides access to
the root of a repository with identifier `«repo_id»` via the URI
`«nexus»/content/repositories/«repo_id»/`, and publishes a link to the
repository in the *Repository Path* column of the *Repositories* page.  For
example, if a Nexus server has URI `https://nexus.levigo.de/` and contains a
repository with name *Infrastructure* and identifier `infrastructure` that
holds a release artifact with

*	group identifier `com.levigo.jira`,

*	artifact identifier `server`,

*	version `287`,

*	classifier `staging`, and

*	packaging `tar.gz`.

then

*	`https://nexus.levigo.de/` indicates the server,

*	`https://nexus.levigo.de/content/repositories/infrastructure/` indicates
	the repository,

*	`https://nexus.levigo.de/content/repositories/infrastructure/com/levigo/jira/server/`
	indicates the artifact version directory of the artifact,

*	`https://nexus.levigo.de/content/repositories/infrastructure/com/levigo/jira/server/maven-metadata.xml`
	indicates the metadata file of the artifact,

*	`https://nexus.levigo.de/content/repositories/infrastructure/com/levigo/jira/server/287/`
	indicates the artifact directory of the artifact,

*	`https://nexus.levigo.de/content/repositories/infrastructure/com/levigo/jira/server/287/server-287.pom`
	indicates the POM file of the artifact, and

*	`https://nexus.levigo.de/content/repositories/infrastructure/com/levigo/jira/server/287/server-287-staging.tar.gz`
	indicates the artifact itself.

If a Maven repository server provides HTTP or HTTPS access to an artifact
comprising a `gzip` compressed `tar` archive then this type attempts to
download the artifact and install the contents of the archive into an
installation directory on the local host.  An installation directory should
ordinarily either be empty (indicating that no archive is currently installed)
or have a `current` subdirectory that contains the *currently installed
version* of the archive and possibly a `back_up` subdirectory that contains the
version of the archive installed prior to installing the currently installed
version.  Each directory, if it exists, contains a `.version` file that
contains a string, extracted from the Maven repository, denoting the version of
the archive the directory contains.  In order to install the contents of an
archive into the installation directory `«install_dir»`, the type

*	writes the content of the appropriate artifact into the
	`«install_dir»/staging.tgz` file,

*	decompresses and unpacks the contents of the `«install_dir»/staging.tgz»`
	file into the `«install_dir»/staging` directory,

*	writes the version of the archive into the
	`«install_dir»/staging/.version` file,

*	optionally gives a specified host user and/or group ownership of the
	`«install_dir»/staging` directory and its contents.

*	deletes the `«install_dir»/staging.tgz` file,

*	deletes the `«install_dir»/back_up` directory (if it exists),

*	renames the `«install_dir»/current` directory (if it exists)
	`«install_dir»/back_up`, and

*	renames the `«install_dir»/staging` directory `«install_dir»/current`.

If a step fails then all subsequent steps are abandoned.  The back-up directory
provides limited rollback.

## Setup

The `targz` provider requires the

*	*httpclient* and

*	*nokogiri*

Ruby modules and an external `tar` executable on the executable search path that
can execute

	tar -xzf «file» -C «directory» --strip-components «count»

to decompress and unpack the `gzip` compressed `tar` archive `«file»` into the
directory `«directory»`, stripping the `«count»` leading directories from the
unpacked archive.

## Usage

Suppose that a Maven repository server has *Infrastructure (Testing)* and
*Infrastructure (Production)* repositories holding various versions of various
release artifacts for installation to testing and production hosts
respectively, provides anonymous access to the testing repository via the URI

*	`https://nexus.levigo.de/content/repositories/infrastructure-testing/`,

and provides authenticated access for a Nexus user with name `infrastructure`
and password `1ltS0bkG` to the production repository via the URI

*	`https://nexus.levigo.de/content/repositories/infrastructure-production/`.

Suppose further that both repositories hold various versions of release
artifacts for a customised Jira server, the artifacts are `gzip` compressed
`tar` archives with

*	group identifier `com.levigo.infrastructure` and

*	artifact identifier `jira.server`,

and the artifacts on the testing repository also have

*	classifier `testing` and

*	packaging `tar.gz`

even though their POM files give packaging `pom`.

### Installation to a development workstation

----

As the `fred` user on a development workstation, I want to install the latest
version of the testing Jira server to the local `/home/fred/jira` directory.

----

Apply

	maven_package { "jira-development":
	  state            => up_to_date,
	  install_dir_path => "/home/fred/jira",
	  repo_uri         => "https://nexus.levigo.de/content/repositories/infrastructure-testing/",
	  group_id         => "com.levigo.infrastructure",
	  artifact_id      => "jira.server",
	  classifier       => "testing",
	  packaging        => "tar.gz"
	}

### Removal from a development workstation

----

As the `fred` user on a development workstation, I want to remove the testing
Jira server I previously installed in the local `/home/fred/jira` directory.

----

Apply

	maven_package { "jira-development":
	  state            => absent,
	  install_dir_path => "/home/fred/jira",
	}

### Installation to a production host

----

As the `root` user on a production host, I want to install the latest version
of the production Jira server to the local `/opt/jira` directory and give the
local `jira` user and `jira` group ownership.

----

Apply

	maven_package { "jira-production":
	  state              => up_to_date,
	  install_dir_path   => "/opt/jira",
	  install_user       => "jira",
	  install_group      => "jira"
	  repo_uri           => "https://nexus.levigo.de/content/repositories/continuous-deployment-production",
	  repo_user_name     => "infrastructure",
	  repo_user_password => "1ltS0bkG",
	  group_id           => "com.levigo.infrastructure",
	  artifact_id        => "jira.server",
	}

## Reference

### Parameters

#### `name` *namevar*

A name to identify the Puppet resource. Required.

#### `state`

The state of the installation directory. One of

*	`absent` - the installation directory is empty.

*	`corrupt` - the installation directory is not empty but the
	`current` subdirectory does not contain the contents of an archive (the
	`.version` file of the `current` subdirectory is unreadable). The
	resource uses this state to ensure that the installation directory is
	empty if a user specifies `state => absent`. It is not meant for use
	by a user, but if a user *does* specify `state => corrupt` then nothing
	happens.

*	`present` - the `current` subdirectory contains the contents of an
	archive (the `.version` file of the `current` subdirectory is
	readable).

*	`up_to_date` - the `current` subdirectory contains the contents of
	an up-to-date archive (the `.version` file of the `current`
	subdirectory contains the version string of the artifact that provides
	the latest version of the archive).

#### `install_dir_path`

A path to the installation directory. Required if state is set.

#### `install_user`

The name or UID of the local user that owns the `current` subdirectory of the
installation directory and all of its contents.

#### `install_group`

The name or GID of the local group that owns the `current` subdirectory of the
installation directory and all of its contents.

#### `repo_uri`

The URI of the repository. Required if state is "present" or "up_to_date".

#### `repo_user_name`

The name of the authenticated repository user.

#### `repo_user_password`

The password of the authenticated repository user.

#### `group_id`

The group identifier of the artifact. Required if state is "present" or
"up_to_date".

#### `artifact_id`

The artifact identifier of the artifact. Required if state is "present" or
"up_to_date".

#### `classifier`

The classifier of the artifact.

#### `packaging`

The packaging of the artifact. If this parameter is not given then the
packaging is deduced from the POM file.

#### `strip_components`

The number of leading directory components to strip from the unpacked 
archive. The default is 0.

## Limitations

The type can only download release artifacts, not snapshots.

The type can only install artifacts that comprise a `gzip` compressed `tar`
archive.

The type uses an external `tar` executable to decompress and unpack the
`stage.tgz` file because I have not found a native Ruby `tar` implementation
that can handle long archive entry names.

